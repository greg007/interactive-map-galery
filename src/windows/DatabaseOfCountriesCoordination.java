package windows;

import java.awt.Point;

public class DatabaseOfCountriesCoordination {
	
	private RecordCountryAndPosition[] countries;
	private int count;
	
	public DatabaseOfCountriesCoordination() {
		countries = new RecordCountryAndPosition[1000];
				
		// Europe
		countries[count++] = new RecordCountryAndPosition(new Point(2094, 609),0); // albania
		countries[count++] = new RecordCountryAndPosition(new Point(1864, 591),1); // Andorra
		countries[count++] = new RecordCountryAndPosition(new Point(2409, 625),2); // Armenia
		countries[count++] = new RecordCountryAndPosition(new Point(2025, 513),3); // austria
		countries[count++] = new RecordCountryAndPosition(new Point(1993, 516),3); // austria
		countries[count++] = new RecordCountryAndPosition(new Point(2460, 628),4); // Azerbaijan
		countries[count++] = new RecordCountryAndPosition(new Point(2435, 626),4); // Azerbaijan
		countries[count++] = new RecordCountryAndPosition(new Point(2182, 414),5); // Belarus
		countries[count++] = new RecordCountryAndPosition(new Point(1906, 470),6); // Belgium
		countries[count++] = new RecordCountryAndPosition(new Point(2064, 567),7); // Bosnia and Herzegovina
		countries[count++] = new RecordCountryAndPosition(new Point(2156, 592),8); // Bulgaria
		countries[count++] = new RecordCountryAndPosition(new Point(2034, 553),9); // Croatia
		countries[count++] = new RecordCountryAndPosition(new Point(2048, 541),9); // Croatia
		countries[count++] = new RecordCountryAndPosition(new Point(2268, 710),10); // Cyprus
		countries[count++] = new RecordCountryAndPosition(new Point(2026, 479),11); // czech
		countries[count++] = new RecordCountryAndPosition(new Point(1958, 386),12); // Denmark
		countries[count++] = new RecordCountryAndPosition(new Point(1988, 398),12); // Denmark
		countries[count++] = new RecordCountryAndPosition(new Point(1985, 407),12); // Denmark
		countries[count++] = new RecordCountryAndPosition(new Point(2116, 349),13); // Estonia
		countries[count++] = new RecordCountryAndPosition(new Point(2146, 349),13); // Estonia
		countries[count++] = new RecordCountryAndPosition(new Point(2091, 211),14); // Finland
		countries[count++] = new RecordCountryAndPosition(new Point(2137, 233),14); // Finland
		countries[count++] = new RecordCountryAndPosition(new Point(2142, 289),14); // Finland
		countries[count++] = new RecordCountryAndPosition(new Point(1859, 506),15); // France
		countries[count++] = new RecordCountryAndPosition(new Point(1883, 551),15); // France
		countries[count++] = new RecordCountryAndPosition(new Point(1956, 597),15); // France
		countries[count++] = new RecordCountryAndPosition(new Point(2389, 596),16); // Georgia
		countries[count++] = new RecordCountryAndPosition(new Point(1972, 468),17); // germany
		countries[count++] = new RecordCountryAndPosition(new Point(2163, 615),18); // Greece
		countries[count++] = new RecordCountryAndPosition(new Point(2116, 632),18); // Greece
		countries[count++] = new RecordCountryAndPosition(new Point(2125, 672),18); // Greece
		countries[count++] = new RecordCountryAndPosition(new Point(2079, 524),19); // Hungary
		countries[count++] = new RecordCountryAndPosition(new Point(1632, 249),20); // Iceland
		countries[count++] = new RecordCountryAndPosition(new Point(1666, 264),20); // Iceland
		countries[count++] = new RecordCountryAndPosition(new Point(1759, 408),21); // Ireland
		countries[count++] = new RecordCountryAndPosition(new Point(1756, 429),21); // Ireland
		countries[count++] = new RecordCountryAndPosition(new Point(1959, 556),22); // Italy
		countries[count++] = new RecordCountryAndPosition(new Point(2036, 610),22); // Italy
		countries[count++] = new RecordCountryAndPosition(new Point(2049, 647),22); // Italy
		countries[count++] = new RecordCountryAndPosition(new Point(2025, 669),22); // Italy
		countries[count++] = new RecordCountryAndPosition(new Point(1957, 627),22); // Italy
		countries[count++] = new RecordCountryAndPosition(new Point(2445, 502),23); // Kazakhstan
		countries[count++] = new RecordCountryAndPosition(new Point(2507, 568),23); // Kazakhstan
		countries[count++] = new RecordCountryAndPosition(new Point(2725, 461),23); // Kazakhstan
		countries[count++] = new RecordCountryAndPosition(new Point(2102, 593),24); // Kosovo
		countries[count++] = new RecordCountryAndPosition(new Point(2113, 376),25); // Latvia
		countries[count++] = new RecordCountryAndPosition(new Point(2150, 378),25); // Latvia
		countries[count++] = new RecordCountryAndPosition(new Point(1980, 520),26); // Liechtenstein
		countries[count++] = new RecordCountryAndPosition(new Point(2128, 399),27); // Lithuania
		countries[count++] = new RecordCountryAndPosition(new Point(1920, 481),28); // Luxembourg
		countries[count++] = new RecordCountryAndPosition(new Point(2114, 605),29); // Macedonia
		countries[count++] = new RecordCountryAndPosition(new Point(2195, 517),31); // Moldova
		countries[count++] = new RecordCountryAndPosition(new Point(2084, 585),33); // Montenegro
		countries[count++] = new RecordCountryAndPosition(new Point(1909, 446),34); // Netherlands
		countries[count++] = new RecordCountryAndPosition(new Point(2145, 189),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(2101, 193),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(2055, 205),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(2030, 224),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(2007, 249),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(1970, 302),35); // Norway
		countries[count++] = new RecordCountryAndPosition(new Point(2068, 444),36); // poland
		countries[count++] = new RecordCountryAndPosition(new Point(1743, 615),37); // Portugal
		countries[count++] = new RecordCountryAndPosition(new Point(2157, 546),38); // Romania
		countries[count++] = new RecordCountryAndPosition(new Point(2190, 216),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2229, 360),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2360, 527),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2434, 403),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2810, 378),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3194, 409),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3505, 563),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3516, 264),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3665, 387),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3694, 296),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3749, 241),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3279, 203),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3101, 183),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2718, 188),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2567, 197),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2320, 230),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3519, 442),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3556, 485),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(3580, 525),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2098, 408),39); // Russia
		countries[count++] = new RecordCountryAndPosition(new Point(2000, 573),40); // San Marino
		countries[count++] = new RecordCountryAndPosition(new Point(2107, 571),41); // Serbia
		countries[count++] = new RecordCountryAndPosition(new Point(2070, 501),42); // slovakia
		countries[count++] = new RecordCountryAndPosition(new Point(2101, 494),42); // slovakia
		countries[count++] = new RecordCountryAndPosition(new Point(2025, 537),43); // Slovenia
		countries[count++] = new RecordCountryAndPosition(new Point(1797, 592),44); // Spain
		countries[count++] = new RecordCountryAndPosition(new Point(1880, 637),44); // Spain
		countries[count++] = new RecordCountryAndPosition(new Point(1948, 520),46); // Switzerland
		countries[count++] = new RecordCountryAndPosition(new Point(2058, 245),45); // Sweden
		countries[count++] = new RecordCountryAndPosition(new Point(2018, 340),45); // Sweden
		countries[count++] = new RecordCountryAndPosition(new Point(2298, 642),47); // Turkey
		countries[count++] = new RecordCountryAndPosition(new Point(2186, 610),47); // Turkey
		countries[count++] = new RecordCountryAndPosition(new Point(2264, 548),48); // Ukraine
		countries[count++] = new RecordCountryAndPosition(new Point(2209, 540),48); // Ukraine
		countries[count++] = new RecordCountryAndPosition(new Point(2186, 485),48); // Ukraine
		countries[count++] = new RecordCountryAndPosition(new Point(1776, 406),49); // United Kingdom
		countries[count++] = new RecordCountryAndPosition(new Point(1800, 373),49); // United Kingdom
		countries[count++] = new RecordCountryAndPosition(new Point(1835, 449),49); // United Kingdom
		

		int index = new ListModel_Countries().getLength(1);
		countries[count++] = new RecordCountryAndPosition(new Point(2748, 688),index+0); // Afghanistan
		countries[count++] = new RecordCountryAndPosition(new Point(2673, 754),index+0); // Afghanistan
		countries[count++] = new RecordCountryAndPosition(new Point(2781, 675),index+0); // Afghanistan
		countries[count++] = new RecordCountryAndPosition(new Point(3027, 854),index+2); // Bangladesh
		countries[count++] = new RecordCountryAndPosition(new Point(3072, 903),index+2); // Bangladesh
		countries[count++] = new RecordCountryAndPosition(new Point(3039, 886),index+2); // Bangladesh
		countries[count++] = new RecordCountryAndPosition(new Point(3036, 825),index+3); // Bhutan
		countries[count++] = new RecordCountryAndPosition(new Point(3406, 1181),index+4); // Brunei
		countries[count++] = new RecordCountryAndPosition(new Point(3266, 1062),index+5); // Cambodia
		countries[count++] = new RecordCountryAndPosition(new Point(2866, 654),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(2929, 563),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3196, 854),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3314, 704),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3415, 534),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3304, 486),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3314, 956),index+6); // China
		countries[count++] = new RecordCountryAndPosition(new Point(3368, 906),index+6); // China - hongkong
		countries[count++] = new RecordCountryAndPosition(new Point(2838, 731),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(2798, 885),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(3075, 877),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(3091, 819),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(3013, 834),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(3064, 883),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(2852, 848),index+7); // India
		countries[count++] = new RecordCountryAndPosition(new Point(3183, 1206),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3238, 1278),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3348, 1262),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3427, 1240),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3748, 1363),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3713, 1314),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3657, 1302),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3652, 1268),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3305, 1360),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3364, 1368),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3427, 1385),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3441, 1391),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3459, 1386),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3481, 1387),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3530, 1404),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3479, 1406),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3469, 1402),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3571, 1307),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3588, 1239),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3585, 1264),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3596, 1302),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3619, 1305),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3535, 1243),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3496, 1241),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3480, 1253),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3496, 1282),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3479, 1319),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(3476, 1291),index+8); // Indonesia
		countries[count++] = new RecordCountryAndPosition(new Point(2620, 813),index+9); // Iran
		countries[count++] = new RecordCountryAndPosition(new Point(2442, 674),index+9); // Iran
		countries[count++] = new RecordCountryAndPosition(new Point(2572, 733),index+9); // Iran
		countries[count++] = new RecordCountryAndPosition(new Point(2397, 728),index+10); // Iraq
		countries[count++] = new RecordCountryAndPosition(new Point(2293, 770),index+11); // Israel
		countries[count++] = new RecordCountryAndPosition(new Point(2299, 746),index+11); // Israel
		countries[count++] = new RecordCountryAndPosition(new Point(3551, 743),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3577, 730),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3578, 706),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3602, 714),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3639, 677),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3625, 579),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3605, 601),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(3599, 588),index+12); // Japan
		countries[count++] = new RecordCountryAndPosition(new Point(2312, 762),index+13); // Jordan
		countries[count++] = new RecordCountryAndPosition(new Point(2465, 792),index+14); // Kuwait
		countries[count++] = new RecordCountryAndPosition(new Point(2733, 634),index+15); // Kyrgyzstan
		countries[count++] = new RecordCountryAndPosition(new Point(2770, 626),index+15); // Kyrgyzstan
		countries[count++] = new RecordCountryAndPosition(new Point(2747, 598),index+15); // Kyrgyzstan
		countries[count++] = new RecordCountryAndPosition(new Point(2796, 599),index+15); // Kyrgyzstan
		countries[count++] = new RecordCountryAndPosition(new Point(3272, 997),index+16); // Laos
		countries[count++] = new RecordCountryAndPosition(new Point(3217, 939),index+16); // Laos
		countries[count++] = new RecordCountryAndPosition(new Point(2306, 726),index+17); // Lebanon
		countries[count++] = new RecordCountryAndPosition(new Point(3231, 1188),index+18); // Malaysia
		countries[count++] = new RecordCountryAndPosition(new Point(3434, 1173),index+18); // Malaysia
		countries[count++] = new RecordCountryAndPosition(new Point(3352, 1233),index+18); // Malaysia
		countries[count++] = new RecordCountryAndPosition(new Point(3394, 1212),index+18); // Malaysia
		countries[count++] = new RecordCountryAndPosition(new Point(3224, 522),index+20); // Mongolia
		countries[count++] = new RecordCountryAndPosition(new Point(3052, 515),index+20); // Mongolia
		countries[count++] = new RecordCountryAndPosition(new Point(3125, 862),index+21); // Myanmar (Burma)
		countries[count++] = new RecordCountryAndPosition(new Point(3125, 928),index+21); // Myanmar (Burma)
		countries[count++] = new RecordCountryAndPosition(new Point(3184, 1063),index+21); // Myanmar (Burma)
		countries[count++] = new RecordCountryAndPosition(new Point(3177, 1038),index+21); // Myanmar (Burma)
		countries[count++] = new RecordCountryAndPosition(new Point(2930, 806),index+22); // Nepal
		countries[count++] = new RecordCountryAndPosition(new Point(2987, 827),index+22); // Nepal
		countries[count++] = new RecordCountryAndPosition(new Point(3463, 613),index+23); // North Korea
		countries[count++] = new RecordCountryAndPosition(new Point(3448, 640),index+23); // North Korea
		countries[count++] = new RecordCountryAndPosition(new Point(2593, 897),index+24); // Oman
		countries[count++] = new RecordCountryAndPosition(new Point(2588, 950),index+24); // Oman
		countries[count++] = new RecordCountryAndPosition(new Point(2790, 697),index+25); // Pakistan
		countries[count++] = new RecordCountryAndPosition(new Point(2773, 759),index+25); // Pakistan
		countries[count++] = new RecordCountryAndPosition(new Point(2718, 834),index+25); // Pakistan
		countries[count++] = new RecordCountryAndPosition(new Point(2300, 760),index+26); // Palestine
		countries[count++] = new RecordCountryAndPosition(new Point(3511, 1131),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3522, 1129),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3546, 1131),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3519, 1093),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3506, 1081),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3538, 1066),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3538, 1088),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3481, 1053),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3509, 1039),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(3474, 1012),index+27); // Philippines
		countries[count++] = new RecordCountryAndPosition(new Point(2518, 860),index+28); // Qatar
		countries[count++] = new RecordCountryAndPosition(new Point(2414, 985),index+29); // Saudi Arabia
		countries[count++] = new RecordCountryAndPosition(new Point(2419, 867),index+29); // Saudi Arabia
		countries[count++] = new RecordCountryAndPosition(new Point(3257, 1228),index+30); // Singapore
		countries[count++] = new RecordCountryAndPosition(new Point(3487, 684),index+31); // South Korea
		countries[count++] = new RecordCountryAndPosition(new Point(2938, 1138),index+32); // Sri Lanka
		countries[count++] = new RecordCountryAndPosition(new Point(2346, 699),index+33); // Syria
		countries[count++] = new RecordCountryAndPosition(new Point(3455, 880),index+34); // Taiwan
		countries[count++] = new RecordCountryAndPosition(new Point(2773, 657),index+35); // Tajikistan
		countries[count++] = new RecordCountryAndPosition(new Point(2722, 647),index+35); // Tajikistan
		countries[count++] = new RecordCountryAndPosition(new Point(2725, 624),index+35); // Tajikistan
		countries[count++] = new RecordCountryAndPosition(new Point(3185, 974),index+36); // Thailand
		countries[count++] = new RecordCountryAndPosition(new Point(3209, 1009),index+36); // Thailand
		countries[count++] = new RecordCountryAndPosition(new Point(3219, 1153),index+36); // Thailand
		countries[count++] = new RecordCountryAndPosition(new Point(3187, 1117),index+36); // Thailand
		countries[count++] = new RecordCountryAndPosition(new Point(3189, 1082),index+36); // Thailand
		countries[count++] = new RecordCountryAndPosition(new Point(3556, 1389),index+37); // Timor-Leste
		countries[count++] = new RecordCountryAndPosition(new Point(2645, 669),index+38); // Turkmenistan
		countries[count++] = new RecordCountryAndPosition(new Point(2547, 639),index+38); // Turkmenistan
		countries[count++] = new RecordCountryAndPosition(new Point(2522, 598),index+38); // Turkmenistan
		countries[count++] = new RecordCountryAndPosition(new Point(2580, 860),index+39); // United Arab Emirates
		countries[count++] = new RecordCountryAndPosition(new Point(2563, 886),index+39); // United Arab Emirates
		countries[count++] = new RecordCountryAndPosition(new Point(2556, 571),index+40); // Uzbekistan
		countries[count++] = new RecordCountryAndPosition(new Point(2683, 640),index+40); // Uzbekistan
		countries[count++] = new RecordCountryAndPosition(new Point(2744, 621),index+40); // Uzbekistan
		countries[count++] = new RecordCountryAndPosition(new Point(2722, 611),index+40); // Uzbekistan
		countries[count++] = new RecordCountryAndPosition(new Point(3243, 912),index+41); // Vietnam
		countries[count++] = new RecordCountryAndPosition(new Point(3255, 950),index+41); // Vietnam
		countries[count++] = new RecordCountryAndPosition(new Point(3284, 992),index+41); // Vietnam
		countries[count++] = new RecordCountryAndPosition(new Point(3310, 1062),index+41); // Vietnam
		countries[count++] = new RecordCountryAndPosition(new Point(3279, 1095),index+41); // Vietnam
		countries[count++] = new RecordCountryAndPosition(new Point(2455, 1016),index+42); // Yemen
		countries[count++] = new RecordCountryAndPosition(new Point(2521, 988),index+42); // Yemen

		index = new ListModel_Countries().getLength(2);
		// america
		// Antigua and Barbuda (America)
		// Bahamas (America)
		countries[count++] = new RecordCountryAndPosition(new Point(791, 870),index+1);
		// Barbados (America)
		// Belize (America)
		countries[count++] = new RecordCountryAndPosition(new Point(631, 980),index+3);
		// Canada (America)
		countries[count++] = new RecordCountryAndPosition(new Point(346, 484),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(846, 570),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1043, 560),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1062, 547),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1038, 503),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1160, 491),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1081, 422),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(915, 514),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(566, 376),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(378, 299),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(944, 193),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1019, 224),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(892, 254),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(978, 264),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(896, 206),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(828, 190),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(777, 169),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(753, 201),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(723, 153),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(790, 113),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(836, 125),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(889, 127),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(897, 100),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(931, 159),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(937, 119),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(969, 121),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(987, 151),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1002, 133),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1019, 114),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(983, 91),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1035, 97),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1052, 126),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1097, 130),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1126, 154),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1098, 78),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1212, 61),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1115, 107),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1027, 171),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1075, 174),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1202, 235),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1127, 260),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1068, 264),index+4);
		countries[count++] = new RecordCountryAndPosition(new Point(1145, 191),index+4);
		// Costa Rica (America)
		countries[count++] = new RecordCountryAndPosition(new Point(684, 1099),index+5);
		// Cuba (America)
		countries[count++] = new RecordCountryAndPosition(new Point(814, 936),index+6);
		countries[count++] = new RecordCountryAndPosition(new Point(801, 929),index+6);
		countries[count++] = new RecordCountryAndPosition(new Point(780, 913),index+6);
		countries[count++] = new RecordCountryAndPosition(new Point(745, 898),index+6);
		countries[count++] = new RecordCountryAndPosition(new Point(713, 901),index+6);
		// Dominica (America)
		// Dominican Republic (America)
		countries[count++] = new RecordCountryAndPosition(new Point(894, 960),index+8);
		countries[count++] = new RecordCountryAndPosition(new Point(874, 958),index+8);
		// El Salvador (America)
		countries[count++] = new RecordCountryAndPosition(new Point(624, 1042),index+9);
		// Grenada (America)
		// Guatemala (America)
		countries[count++] = new RecordCountryAndPosition(new Point(611, 988),index+11);
		countries[count++] = new RecordCountryAndPosition(new Point(602, 1018),index+11);
		// Haiti (America)
		countries[count++] = new RecordCountryAndPosition(new Point(857, 953),index+12);
		countries[count++] = new RecordCountryAndPosition(new Point(854, 968),index+12);
		// Honduras (America)
		countries[count++] = new RecordCountryAndPosition(new Point(645, 1022),index+13);
		countries[count++] = new RecordCountryAndPosition(new Point(685, 1013),index+13);
		// Jamaica (America)
		// Mexico (America)
		countries[count++] = new RecordCountryAndPosition(new Point(356, 879),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(347, 861),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(335, 825),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(316, 752),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(631, 949),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(570, 990),index+15);
		countries[count++] = new RecordCountryAndPosition(new Point(454, 867),index+15);
		// Nicaragua (America)
		countries[count++] = new RecordCountryAndPosition(new Point(663, 1045),index+16);
		// Panama (America)
		countries[count++] = new RecordCountryAndPosition(new Point(767, 1125),index+17);
		countries[count++] = new RecordCountryAndPosition(new Point(749, 1110),index+17);
		countries[count++] = new RecordCountryAndPosition(new Point(726, 1121),index+17);
		countries[count++] = new RecordCountryAndPosition(new Point(708, 1118),index+17);
		// St. Kitts and Nevis (America)
		// St. Lucia (America)
		// St. Vincent and The Grenadines (America)
		// Trinidad and Tobago (America)
		countries[count++] = new RecordCountryAndPosition(new Point(994, 1091),index+21);
		// Alabama (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(673, 768),index+22);
		countries[count++] = new RecordCountryAndPosition(new Point(707, 737),index+22);
		// Alaska (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(301, 250),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(159, 257),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(60, 381),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(116, 318),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(220, 325),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(359, 385),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(355, 350),index+23);
		countries[count++] = new RecordCountryAndPosition(new Point(332, 340),index+23);
		// Arizona (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(376, 720),index+24);
		// Arkansas (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(620, 719),index+25);
		// California (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(287, 647),index+26);
		// Colorado (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(490, 655),index+27);
		// Connecticut (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(920, 607),index+28);
		// Delaware (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(876, 649),index+29);
		// Florida (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(716, 770),index+30);
		countries[count++] = new RecordCountryAndPosition(new Point(749, 795),index+30);
		// Georgia (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(752, 743),index+31);
		// Hawaii (USA) (America)
		// Idaho (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(422, 500),index+33);
		countries[count++] = new RecordCountryAndPosition(new Point(412, 553),index+33);
		countries[count++] = new RecordCountryAndPosition(new Point(420, 593),index+33);
		// Illinois (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(728, 599),index+34);
		// Indiana (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(763, 634),index+35);
		// Iowa (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(653, 606),index+36);
		// Kansas (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(567, 667),index+37);
		// Kentucky (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(737, 675),index+38);
		// Louisiana (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(634, 778),index+39);
		countries[count++] = new RecordCountryAndPosition(new Point(604, 764),index+39);
		// Maine (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(984, 558),index+40);
		// Maryland (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(856, 639),index+41);
		// Massachusetts (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(947, 594),index+42);
		// Michigan (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(784, 578),index+43);
		// Minnesota (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(690, 517),index+44);
		// Mississippi (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(666, 775),index+45);
		countries[count++] = new RecordCountryAndPosition(new Point(659, 743),index+45);
		// Missouri (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(674, 694),index+46);
		countries[count++] = new RecordCountryAndPosition(new Point(649, 659),index+46);
		// Montana (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(476, 522),index+47);
		// Nebraska (NE} (America)
		countries[count++] = new RecordCountryAndPosition(new Point(581, 619),index+48);
		// Nevada (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(352, 636),index+49);
		// New Hampshire (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(944, 580),index+50);
		// New Jersey (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(890, 630),index+51);
		// New Mexico (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(455, 715),index+52);
		// New York (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(890, 596),index+53);
		// North Carolina (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(797, 720),index+54);
		// North Dakota (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(597, 527),index+55);
		// Ohio (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(802, 632),index+56);
		// Oklahoma (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(517, 693),index+57);
		countries[count++] = new RecordCountryAndPosition(new Point(562, 714),index+57);
		// Oregon (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(341, 568),index+58);
		// Pennsylvania (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(842, 622),index+59);
		// Rhode Island (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(933, 604),index+60);
		// South Carolina (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(796, 720),index+61);
		// South Dakota (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(581, 571),index+62);
		// Tennessee (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(721, 704),index+63);
		// Texas (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(479, 771),index+64);
		countries[count++] = new RecordCountryAndPosition(new Point(506, 726),index+64);
		// Utah (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(411, 645),index+65);
		// Vermont (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(932, 562),index+66);
		// Virginia (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(788, 680),index+67);
		countries[count++] = new RecordCountryAndPosition(new Point(848, 667),index+67);
		// Washington (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(367, 523),index+68);
		// West Virginia (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(827, 651),index+69);
		countries[count++] = new RecordCountryAndPosition(new Point(795, 669),index+69);
		// Wisconsin (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(732, 556),index+70);
		// Wyoming (USA) (America)
		countries[count++] = new RecordCountryAndPosition(new Point(484, 598),index+71);
		// Greenland Kalaallit Nunaat (America) // gronsko
		countries[count++] = new RecordCountryAndPosition(new Point(1680, 67),index+72);
		countries[count++] = new RecordCountryAndPosition(new Point(1305, 101),index+72);
		countries[count++] = new RecordCountryAndPosition(new Point(1373, 246),index+72);
		countries[count++] = new RecordCountryAndPosition(new Point(1554, 189),index+72);
		countries[count++] = new RecordCountryAndPosition(new Point(1635, 174),index+72);
		countries[count++] = new RecordCountryAndPosition(new Point(1468, 116),index+72);
		// Argentina (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1071, 2091),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(1007, 2001),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(1041, 1914),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(982, 1893),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(966, 1743),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(1111, 1669),index+73);
		countries[count++] = new RecordCountryAndPosition(new Point(976, 1649),index+73);
		// Bolivia (America)
		countries[count++] = new RecordCountryAndPosition(new Point(920, 1439),index+74);
		countries[count++] = new RecordCountryAndPosition(new Point(964, 1529),index+74);
		// Brazil (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1141, 1702),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(1175, 1516),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(1116, 1239),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(987, 1208),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(904, 1241),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(871, 1350),index+75);
		countries[count++] = new RecordCountryAndPosition(new Point(1150, 1363),index+75);
		// Chile (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1055, 2092),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(1044, 2069),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(1004, 2065),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(975, 2033),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(968, 2004),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(950, 1946),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(924, 1864),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(910, 1800),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(904, 1738),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(903, 1645),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(894, 1553),index+76);
		countries[count++] = new RecordCountryAndPosition(new Point(904, 1618),index+76);
		// Colombia (America)
		countries[count++] = new RecordCountryAndPosition(new Point(835, 1080),index+77);
		countries[count++] = new RecordCountryAndPosition(new Point(871, 1291),index+77);
		countries[count++] = new RecordCountryAndPosition(new Point(818, 1185),index+77);
		// Ecuador (America)
		countries[count++] = new RecordCountryAndPosition(new Point(753, 1272),index+78);
		// Guyana (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1029, 1209),index+79);
		countries[count++] = new RecordCountryAndPosition(new Point(1017, 1153),index+79);
		// Paraguay (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1032, 1593),index+80);
		countries[count++] = new RecordCountryAndPosition(new Point(1089, 1644),index+80);
		// Peru (America)
		countries[count++] = new RecordCountryAndPosition(new Point(813, 1304),index+81);
		countries[count++] = new RecordCountryAndPosition(new Point(722, 1325),index+81);
		countries[count++] = new RecordCountryAndPosition(new Point(837, 1454),index+81);
		// Suriname (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1051, 1185),index+82);
		// Uruguay (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1105, 1758),index+83);
		// Venezuela (America)
		countries[count++] = new RecordCountryAndPosition(new Point(922, 1196),index+84);
		countries[count++] = new RecordCountryAndPosition(new Point(944, 1123),index+84);
		countries[count++] = new RecordCountryAndPosition(new Point(839, 1104),index+84);
		// Falkland Islands (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1160, 2054),index+85);
		countries[count++] = new RecordCountryAndPosition(new Point(1144, 2054),index+85);
		// French Guiana  (America)
		countries[count++] = new RecordCountryAndPosition(new Point(1103, 1200),index+86);
		// South Georgia and South Sandwich Islands (America)
		
		
		index = new ListModel_Countries().getLength(3);
		// australia
		// Western Australia (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3547, 1536),index+0);
		countries[count++] = new RecordCountryAndPosition(new Point(3441, 1612),index+0);
		countries[count++] = new RecordCountryAndPosition(new Point(3375, 1755),index+0);
		// Northern Territory (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3654, 1457),index+1);
		countries[count++] = new RecordCountryAndPosition(new Point(3626, 1566),index+1);
		// South Australia (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3604, 1703),index+2);
		countries[count++] = new RecordCountryAndPosition(new Point(3648, 1771),index+2);
		countries[count++] = new RecordCountryAndPosition(new Point(3624, 1794),index+2);
		// Queensland (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3758, 1603),index+3);
		// New South Wales (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3740, 1751),index+4);
		// Victoria (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3709, 1829),index+5);
		countries[count++] = new RecordCountryAndPosition(new Point(3672, 1822),index+5);
		// Tasmania (Australia) (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3676, 1905),index+6);
		// Fiji (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(4265, 1512),index+7);
		countries[count++] = new RecordCountryAndPosition(new Point(4243, 1532),index+8);
		// Kiribati (Australia)
		// Marshall Islands (Australia)
		// Micronesia (Australia)
		// Nauru (Australia)
		// New Zealand (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(4085, 1809),index+12);
		countries[count++] = new RecordCountryAndPosition(new Point(4104, 1850),index+12);
		countries[count++] = new RecordCountryAndPosition(new Point(4072, 1870),index+12);
		countries[count++] = new RecordCountryAndPosition(new Point(4008, 1905),index+12);
		countries[count++] = new RecordCountryAndPosition(new Point(3926, 1953),index+12);
		// Palau (Australia)
		// Papua New Guinea (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(3789, 1341),index+14);
		countries[count++] = new RecordCountryAndPosition(new Point(3856, 1400),index+14);
		countries[count++] = new RecordCountryAndPosition(new Point(3886, 1347),index+14);
		countries[count++] = new RecordCountryAndPosition(new Point(3917, 1325),index+14);
		countries[count++] = new RecordCountryAndPosition(new Point(3908, 1337),index+14);
		// Samoa (Australia)
		// Solomon Islands (Australia)
		countries[count++] = new RecordCountryAndPosition(new Point(4023, 1402),index+16);
		// Tonga (Australia)
		// Tuvalu (Australia)
		// Vanuatu (Australia)
		
		index = new ListModel_Countries().getLength(4);
		// africa
		countries[count++] = new RecordCountryAndPosition(new Point(1878, 710),index+0); // Algeria
		countries[count++] = new RecordCountryAndPosition(new Point(1906, 865),index+0); // Algeria
		countries[count++] = new RecordCountryAndPosition(new Point(2035, 1382),index+1); // Angola
		countries[count++] = new RecordCountryAndPosition(new Point(2059, 1436),index+1); // Angola
		countries[count++] = new RecordCountryAndPosition(new Point(1865, 1091),index+2); // Benin
		countries[count++] = new RecordCountryAndPosition(new Point(1859, 1135),index+2); // Benin
		countries[count++] = new RecordCountryAndPosition(new Point(2135, 1614),index+3); // Botswana
		countries[count++] = new RecordCountryAndPosition(new Point(1818, 1052),index+4); // Burkina Faso
		countries[count++] = new RecordCountryAndPosition(new Point(1772, 1082),index+4); // Burkina Faso
		countries[count++] = new RecordCountryAndPosition(new Point(2241, 1304),index+5); // Burundi
		countries[count++] = new RecordCountryAndPosition(new Point(1977, 1190),index+7); // Cameroon
		countries[count++] = new RecordCountryAndPosition(new Point(2030, 1087),index+7); // Cameroon
		countries[count++] = new RecordCountryAndPosition(new Point(2029, 1055),index+7); // Cameroon
		countries[count++] = new RecordCountryAndPosition(new Point(2141, 1146),index+8); // Central African Republic
		countries[count++] = new RecordCountryAndPosition(new Point(2057, 1164),index+8); // Central African Republic
		countries[count++] = new RecordCountryAndPosition(new Point(2073, 932),index+9); // Chad
		countries[count++] = new RecordCountryAndPosition(new Point(2059, 1115),index+9); // Chad
		countries[count++] = new RecordCountryAndPosition(new Point(2081, 1028),index+9); // Chad
		countries[count++] = new RecordCountryAndPosition(new Point(2009, 1312),index+11); // Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2039, 1293),index+11); // Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2065, 1224),index+11); // Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2019, 1223),index+11); // Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2049, 1252),index+11); // Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2019, 1335),index+12); // Democratic Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2230, 1449),index+12); // Democratic Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2183, 1362),index+12); // Democratic Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(2142, 1258),index+12); // Democratic Republic of the Congo
		countries[count++] = new RecordCountryAndPosition(new Point(1746, 1138),index+13); // Cote d'Ivoire
		countries[count++] = new RecordCountryAndPosition(new Point(2414, 1072),index+14); // Djibouti
		countries[count++] = new RecordCountryAndPosition(new Point(2276, 785),index+15); // Egypt
		countries[count++] = new RecordCountryAndPosition(new Point(2220, 840),index+15); // Egypt
		countries[count++] = new RecordCountryAndPosition(new Point(1974, 1228),index+16); // Equatorial Guinea
		countries[count++] = new RecordCountryAndPosition(new Point(2347, 1003),index+17); // Eritrea
		countries[count++] = new RecordCountryAndPosition(new Point(2411, 1053),index+17); // Eritrea
		countries[count++] = new RecordCountryAndPosition(new Point(2403, 1043),index+17); // Eritrea
		countries[count++] = new RecordCountryAndPosition(new Point(2390, 1030),index+17); // Eritrea
		countries[count++] = new RecordCountryAndPosition(new Point(2350, 1075),index+18); // Ethiopia
		countries[count++] = new RecordCountryAndPosition(new Point(2381, 1153),index+18); // Ethiopia
		countries[count++] = new RecordCountryAndPosition(new Point(2001, 1249),index+19); // Gabon
		countries[count++] = new RecordCountryAndPosition(new Point(1975, 1285),index+19); // Gabon
		countries[count++] = new RecordCountryAndPosition(new Point(1614, 1043),index+20); // Gambia
		countries[count++] = new RecordCountryAndPosition(new Point(1624, 1040),index+20); // Gambia
		countries[count++] = new RecordCountryAndPosition(new Point(1634, 1045),index+20); // Gambia
		countries[count++] = new RecordCountryAndPosition(new Point(1818, 1121),index+21); // Ghana
		countries[count++] = new RecordCountryAndPosition(new Point(1707, 1108),index+22); // Guinea
		countries[count++] = new RecordCountryAndPosition(new Point(1663, 1071),index+22); // Guinea
		countries[count++] = new RecordCountryAndPosition(new Point(1701, 1078),index+22); // Guinea
		countries[count++] = new RecordCountryAndPosition(new Point(1627, 1066),index+23); // Guinea-Bissau
		countries[count++] = new RecordCountryAndPosition(new Point(2358, 1229),index+24); // Kenya
		countries[count++] = new RecordCountryAndPosition(new Point(2207, 1714),index+25); // Lesotho
		countries[count++] = new RecordCountryAndPosition(new Point(1696, 1149),index+26); // Liberia
		countries[count++] = new RecordCountryAndPosition(new Point(2131, 840),index+27); // Libya
		countries[count++] = new RecordCountryAndPosition(new Point(2004, 826),index+27); // Libya
		countries[count++] = new RecordCountryAndPosition(new Point(2501, 1476),index+28); // Madagascar
		countries[count++] = new RecordCountryAndPosition(new Point(2458, 1576),index+28); // Madagascar
		countries[count++] = new RecordCountryAndPosition(new Point(2299, 1408),index+29); // Malawi
		countries[count++] = new RecordCountryAndPosition(new Point(2298, 1432),index+29); // Malawi
		countries[count++] = new RecordCountryAndPosition(new Point(2292, 1463),index+29); // Malawi
		countries[count++] = new RecordCountryAndPosition(new Point(2309, 1493),index+29); // Malawi
		countries[count++] = new RecordCountryAndPosition(new Point(1808, 971),index+30); // Mali
		countries[count++] = new RecordCountryAndPosition(new Point(1685, 1035),index+30); // Mali
		countries[count++] = new RecordCountryAndPosition(new Point(1736, 1067),index+30); // Mali
		countries[count++] = new RecordCountryAndPosition(new Point(1732, 848),index+31); // Mauritania
		countries[count++] = new RecordCountryAndPosition(new Point(1689, 946),index+31); // Mauritania
		countries[count++] = new RecordCountryAndPosition(new Point(1700, 815),index+33); // Morocco
		countries[count++] = new RecordCountryAndPosition(new Point(1767, 738),index+33); // Morocco
		countries[count++] = new RecordCountryAndPosition(new Point(2342, 1464),index+34); // Mozambique
		countries[count++] = new RecordCountryAndPosition(new Point(2282, 1489),index+34); // Mozambique
		countries[count++] = new RecordCountryAndPosition(new Point(2269, 1628),index+34); // Mozambique
		countries[count++] = new RecordCountryAndPosition(new Point(2298, 1538),index+34); // Mozambique
		countries[count++] = new RecordCountryAndPosition(new Point(2156, 1530),index+35); // Namibia
		countries[count++] = new RecordCountryAndPosition(new Point(2122, 1533),index+35); // Namibia
		countries[count++] = new RecordCountryAndPosition(new Point(2065, 1571),index+35); // Namibia
		countries[count++] = new RecordCountryAndPosition(new Point(1869, 1031),index+36); // Niger
		countries[count++] = new RecordCountryAndPosition(new Point(1971, 967),index+36); // Niger
		countries[count++] = new RecordCountryAndPosition(new Point(1934, 1085),index+37); // Nigeria
		countries[count++] = new RecordCountryAndPosition(new Point(2242, 1282),index+38); // Rwanda
		countries[count++] = new RecordCountryAndPosition(new Point(1646, 1023),index+40); // Senegal
		countries[count++] = new RecordCountryAndPosition(new Point(1615, 1053),index+40); // Senegal
		countries[count++] = new RecordCountryAndPosition(new Point(1661, 1051),index+40); // Senegal
		countries[count++] = new RecordCountryAndPosition(new Point(1667, 1119),index+42); // Sierra Leone
		countries[count++] = new RecordCountryAndPosition(new Point(2496, 1109),index+43); // Somalia
		countries[count++] = new RecordCountryAndPosition(new Point(2447, 1202),index+43); // Somalia
		countries[count++] = new RecordCountryAndPosition(new Point(2107, 1655),index+44); // South Africa
		countries[count++] = new RecordCountryAndPosition(new Point(2068, 1706),index+44); // South Africa
		countries[count++] = new RecordCountryAndPosition(new Point(2149, 1744),index+44); // South Africa
		countries[count++] = new RecordCountryAndPosition(new Point(2237, 1687),index+44); // South Africa
		countries[count++] = new RecordCountryAndPosition(new Point(2227, 1638),index+44); // South Africa
		countries[count++] = new RecordCountryAndPosition(new Point(2247, 1150),index+45); // South Sudan
		countries[count++] = new RecordCountryAndPosition(new Point(2270, 1106),index+45); // South Sudan
		countries[count++] = new RecordCountryAndPosition(new Point(2215, 1033),index+46); // Sudan
		countries[count++] = new RecordCountryAndPosition(new Point(2253, 1665),index+47); // Swaziland
		countries[count++] = new RecordCountryAndPosition(new Point(2261, 1283),index+48); // Tanzania
		countries[count++] = new RecordCountryAndPosition(new Point(2332, 1373),index+48); // Tanzania
		countries[count++] = new RecordCountryAndPosition(new Point(1838, 1087),index+49); // Togo
		countries[count++] = new RecordCountryAndPosition(new Point(1842, 1103),index+49); // Togo
		countries[count++] = new RecordCountryAndPosition(new Point(1845, 1135),index+49); // Togo
		countries[count++] = new RecordCountryAndPosition(new Point(1961, 687),index+50); // Tunisia
		countries[count++] = new RecordCountryAndPosition(new Point(1963, 769),index+50); // Tunisia
		countries[count++] = new RecordCountryAndPosition(new Point(1960, 735),index+50); // Tunisia
		countries[count++] = new RecordCountryAndPosition(new Point(2264, 1242),index+51); // Uganda
		countries[count++] = new RecordCountryAndPosition(new Point(2284, 1211),index+51); // Uganda
		countries[count++] = new RecordCountryAndPosition(new Point(2255, 1420),index+52); // Zambia
		countries[count++] = new RecordCountryAndPosition(new Point(2184, 1489),index+52); // Zambia
		countries[count++] = new RecordCountryAndPosition(new Point(2241, 1549),index+53); // Zimbabwe
		countries[count++] = new RecordCountryAndPosition(new Point(1698, 835),index+54); // Western Sahara
		countries[count++] = new RecordCountryAndPosition(new Point(1658, 868),index+54); // Western Sahara
		countries[count++] = new RecordCountryAndPosition(new Point(1628, 906),index+54); // Western Sahara
		//System.out.println("count "+count);
	}
	
	public Point getPoint(int i){
		return countries[i].source;
	}
	
	public int getId(int i){
		return countries[i].countrId;
	}
	
	public int getLength(){
		//return countries.length;
		return count;
	}

}
