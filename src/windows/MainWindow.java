package windows;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.Font;

public class MainWindow implements ActionListener, KeyListener, MouseListener{

	private JFrame frmInteractiveWorldMap;
	private JPanel panel;
	private JButton btnMap;
	private JButton btnCountries;
	private JButton btnHelp;
	private JButton btnAbout;
	private JButton btnExit;
	private JLayeredPane layeredPane;
	private WorldMap_JPanel world_map_panel;
	private static MainWindow window;
	private JPanel list_countries_panel;
	private JPanel help_panel;
	private JPanel about_panel;
	private JPanel galery_panel;
	private PhotoShowPanel photo_show_panel;
	private JPanel photo_show_panel_container;
	private PhotoAddPanel photo_add_panel;
	private JList<String> list;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	private JTextField SearcheField;
	private JTextPane txtAbout;
	private JLabel lblAbout;
	private JLabel lblHelp;
	private JTextPane txtHelp;
	private JLabel lblGaleryName;
	private Constraint_panel constraint_panel;
	private JButton btnCreate;
	private JButton btnModify;
	private JButton btnLeftPage;
	private JButton btnRightPage;
	private JButton btnLeftPhoto;
	private JButton btnRightPhoto;
	private JButton btnGalery;
	
	
	
	public static final String path_to_photos = "data";
	
	private int index_of_country_for_create;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new MainWindow();
					window.frmInteractiveWorldMap.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmInteractiveWorldMap = new JFrame();
		frmInteractiveWorldMap.setTitle("Interactive world map galery");
		frmInteractiveWorldMap.setBounds(100, 100, 658, 523);
		frmInteractiveWorldMap.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frmInteractiveWorldMap.addComponentListener(new ComponentAdapter() 
		{  
		        public void componentResized(ComponentEvent evt) {
		            world_map_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            list_countries_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            help_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            about_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            galery_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            photo_show_panel_container.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		            photo_add_panel.setBounds(0,0,layeredPane.getWidth(),layeredPane.getHeight());
		        }
		});
		

		
		panel = new JPanel();
		frmInteractiveWorldMap.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		btnMap = new JButton("Map");
		btnMap.setToolTipText("Show world map");
		panel.add(btnMap);
		btnMap.addActionListener(this);
		
		btnCountries = new JButton("Countries");
		btnCountries.setToolTipText("Show list of countries");
		panel.add(btnCountries);
		btnCountries.addActionListener(this);
		
		btnHelp = new JButton("Help");
		panel.add(btnHelp);
		btnHelp.addActionListener(this);
		
		btnAbout = new JButton("About");
		panel.add(btnAbout);
		btnAbout.addActionListener(this);
		
		btnExit = new JButton("Exit");
		btnExit.setToolTipText("Exit application");
		panel.add(btnExit);
		btnExit.addActionListener(this);
		
		
		layeredPane = new JLayeredPane();
		frmInteractiveWorldMap.getContentPane().add(layeredPane, BorderLayout.CENTER);
		
		world_map_panel = new WorldMap_JPanel(this);
		world_map_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(world_map_panel);
		
		list_countries_panel = new JPanel();
		list_countries_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(list_countries_panel);
		list_countries_panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		list = new JList<String>();
		scrollPane.setViewportView(list);
		list.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		list.setValueIsAdjusting(true);
		list.setModel(new ListModel_Countries());
		list.addMouseListener(this);
		list.addKeyListener(this);
		
		list_countries_panel.add(scrollPane, BorderLayout.CENTER);
		
		panel_1 = new JPanel();
		list_countries_panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		lblNewLabel = new JLabel("Search");
		panel_1.add(lblNewLabel, BorderLayout.WEST);
		
		SearcheField = new JTextField();
		panel_1.add(SearcheField, BorderLayout.CENTER);
		SearcheField.setColumns(10);
		//SearcheField.addKeyListener(this);
		
		help_panel = new JPanel();
		help_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(help_panel);
		help_panel.setLayout(new BorderLayout(0, 0));
		
		lblHelp = new JLabel("Help");
		lblHelp.setHorizontalAlignment(SwingConstants.CENTER);
		lblHelp.setFont(new Font("Dialog", Font.BOLD, 18));
		help_panel.add(lblHelp, BorderLayout.NORTH);
		
		txtHelp = new JTextPane();
		txtHelp.setEditable(false);
		txtHelp.setText("This application is easy to use. So don't be fool and use it :D");
		help_panel.add(txtHelp, BorderLayout.CENTER);
		
		about_panel = new JPanel();
		about_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(about_panel);
		about_panel.setLayout(new BorderLayout(0, 0));
		
		txtAbout = new JTextPane();
		txtAbout.setEditable(false);
		txtAbout.setText("Thanks for using this application\nHomepage of this application is https://gitlab.com/greg007/interactive-map-galery\nSources is also available on this link.");
		about_panel.add(txtAbout, BorderLayout.CENTER);
		
		lblAbout = new JLabel("About Interactive map galery 1.0");
		lblAbout.setFont(new Font("Dialog", Font.BOLD, 18));
		lblAbout.setHorizontalAlignment(SwingConstants.CENTER);
		about_panel.add(lblAbout, BorderLayout.NORTH);
		
		galery_panel = new JPanel();
		galery_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(galery_panel);
		galery_panel.setLayout(new BorderLayout(0, 0));
		
		lblGaleryName = new JLabel("New label");
		lblGaleryName.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGaleryName.setHorizontalAlignment(SwingConstants.CENTER);
		galery_panel.add(lblGaleryName, BorderLayout.NORTH);
		
		constraint_panel = new Constraint_panel(this);
		galery_panel.add(constraint_panel, BorderLayout.CENTER);
		constraint_panel.setLayout(new BorderLayout(0, 0));

		photo_show_panel_container = new JPanel(){
			private static final long serialVersionUID = 1L;
			public void setVisible(boolean flag){
				super.setVisible(flag);
				photo_show_panel.setVisible(flag);
			}
		};
		photo_show_panel_container.setBounds(0, 0, 656, 458);
		layeredPane.add(photo_show_panel_container);
		photo_show_panel_container.setLayout(new BorderLayout());

		btnLeftPhoto = new JButton("<");
		photo_show_panel_container.add(btnLeftPhoto, BorderLayout.WEST);
		btnLeftPhoto.addActionListener(this);
		
		btnRightPhoto = new JButton(">");
		photo_show_panel_container.add(btnRightPhoto, BorderLayout.EAST);
		btnRightPhoto.addActionListener(this);
		
		photo_show_panel = new PhotoShowPanel(this);
		photo_show_panel_container.add(photo_show_panel, BorderLayout.CENTER);
		
		photo_add_panel = new PhotoAddPanel(this);
		photo_add_panel.setBounds(0, 0, 656, 458);
		layeredPane.add(photo_add_panel);
		
		btnCreate = new JButton("Create galery");
		galery_panel.add(btnCreate, BorderLayout.SOUTH);
		
		btnModify = new JButton("Modify galery");
		galery_panel.add(btnModify, BorderLayout.SOUTH);
		btnModify.addActionListener(this);
		btnCreate.addActionListener(this);
		
		btnLeftPage = new JButton("<");
		galery_panel.add(btnLeftPage,BorderLayout.WEST);
		btnLeftPage.addActionListener(this);
		
		btnRightPage = new JButton(">");
		galery_panel.add(btnRightPage,BorderLayout.EAST);
		btnRightPage.addActionListener(this);
		
		btnGalery = new JButton("Back to gallery");
		
		index_of_country_for_create = -1;
		

		world_map_panel.addKeyListener(this);
		
		// first view
		list_countries_panel.setVisible(false);
		about_panel.setVisible(false);
		help_panel.setVisible(false);
		galery_panel.setVisible(false);
		photo_add_panel.setVisible(false);
		photo_show_panel_container.setVisible(false);

		world_map_panel.setVisible(true);
	}
	
	public void setVisibleGaleryBtn(boolean flag, String hint){
		if (flag){
			panel.add(btnGalery);
			btnGalery.addActionListener(this);
			btnGalery.setToolTipText(hint);
		}else{
			panel.remove(btnGalery);
			btnGalery.removeActionListener(this);
		}
		panel.repaint();
	}
	
	public void openGalery(int index){

		String name_of_directory = list.getModel().getElementAt(index);
		constraint_panel.setDirectory(name_of_directory);
		list_countries_panel.setVisible(false);
		layeredPane.moveToFront(galery_panel);
		galery_panel.setVisible(true);	
		lblGaleryName.setText(list.getModel().getElementAt(index));

		btnLeftPage.setEnabled(false);
		btnRightPage.setEnabled(false);
		
		constraint_panel.resetStartId();
		
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
		if (name_of_directory_fp.exists()) {
			btnCreate.setVisible(false);
			btnModify.setVisible(true);
			galery_panel.add(btnModify, BorderLayout.SOUTH);
			
			btnLeftPage.setEnabled(constraint_panel.canDecrementStartId());
			btnRightPage.setEnabled(constraint_panel.canIncrementStartId());
		}else{
			// if not found galery
			btnModify.setVisible(false);
			btnCreate.setVisible(true);
			galery_panel.add(btnCreate, BorderLayout.SOUTH);
			index_of_country_for_create = index;
		}
	}

	public void actionPerformed(ActionEvent e) {
		world_map_panel.setVisible(false);
		list_countries_panel.setVisible(false);
		about_panel.setVisible(false);
		help_panel.setVisible(false);
		galery_panel.setVisible(false);
		photo_add_panel.setVisible(false);
		photo_show_panel_container.setVisible(false);
		if (e.getSource().equals(btnExit)){
			frmInteractiveWorldMap.dispose();
		}else if(e.getSource().equals(btnMap)){
			// restart position and zoom of map
			world_map_panel.resetView();
			world_map_panel.setVisible(true);
			layeredPane.moveToFront(world_map_panel);
			world_map_panel.addMouseListener(world_map_panel);
			world_map_panel.addMouseMotionListener(world_map_panel);
			world_map_panel.addMouseWheelListener(world_map_panel);
		}else if(e.getSource().equals(btnCountries)){
			list_countries_panel.setVisible(true);
			layeredPane.moveToFront(list_countries_panel);
		}else if(e.getSource().equals(btnAbout)){
			about_panel.setVisible(true);
			layeredPane.moveToFront(about_panel);
		}else if(e.getSource().equals(btnHelp)){
			help_panel.setVisible(true);
			layeredPane.moveToFront(help_panel);
		}else if(e.getSource().equals(btnCreate)){
			createGalery();
			photo_add_panel.setDirectory(lblGaleryName.getText());
			photo_add_panel.setVisible(true);
			layeredPane.moveToFront(photo_add_panel);
		}else if(e.getSource().equals(btnModify)){
			photo_add_panel.setDirectory(lblGaleryName.getText());
			photo_add_panel.setVisible(true);
			layeredPane.moveToFront(photo_add_panel);
		}else if(e.getSource().equals(btnLeftPage)){
			constraint_panel.decrementStartId();
			galery_panel.setVisible(true);
			layeredPane.moveToFront(galery_panel);
			btnLeftPage.setEnabled(constraint_panel.canDecrementStartId());
			btnRightPage.setEnabled(constraint_panel.canIncrementStartId());
		}else if(e.getSource().equals(btnRightPage)){
			constraint_panel.incrementStartId();
			galery_panel.setVisible(true);
			layeredPane.moveToFront(galery_panel);
			btnLeftPage.setEnabled(constraint_panel.canDecrementStartId());
			btnRightPage.setEnabled(constraint_panel.canIncrementStartId());
		}else if (e.getSource().equals(btnRightPhoto)){
			photo_show_panel.incrementId();;
			btnLeftPhoto.setEnabled(photo_show_panel.canDecrementId());
			btnRightPhoto.setEnabled(photo_show_panel.canIncrementId());
			photo_show_panel_container.setVisible(true);
			layeredPane.moveToFront(photo_show_panel_container);
		}else if (e.getSource().equals(btnLeftPhoto)){
			photo_show_panel.decrementId();
			btnLeftPhoto.setEnabled(photo_show_panel.canDecrementId());
			btnRightPhoto.setEnabled(photo_show_panel.canIncrementId());
			photo_show_panel_container.setVisible(true);
			layeredPane.moveToFront(photo_show_panel_container);
		}else if(e.getSource().equals(btnGalery)){
			layeredPane.moveToFront(galery_panel);
			galery_panel.setVisible(true);
		}
	}

	private void createGalery() {
		String name_of_directory = list.getModel().getElementAt(index_of_country_for_create);

		File path_to_photos_fp = new File(path_to_photos);
		if (!path_to_photos_fp.exists()) {
		    try{
		    	path_to_photos_fp.mkdir();
		    } 
		    catch(SecurityException se){
		        System.err.println("cannot create directory "+path_to_photos);
		    }
		}
		
		File name_of_directory_fp = new File(path_to_photos+"/"+name_of_directory);
		if (!name_of_directory_fp.exists()) {
		    try{
		    	name_of_directory_fp.mkdir();
		    } 
		    catch(SecurityException se){
		        System.err.println("cannot create directory "+path_to_photos+"/"+name_of_directory);
		    }
		}
		
		
	}

	public void keyPressed(KeyEvent e) {
		System.out.println("jop1");
		if (galery_panel.isVisible()){
			System.out.println("nop");
			System.out.println(e.getID());
			System.out.println(e.getKeyChar());
			System.out.println(e.getKeyCode());
		}
	}

	public void keyReleased(KeyEvent e) {

		System.out.println("jop2");
	}

	@SuppressWarnings("rawtypes")
	public void keyTyped(KeyEvent e) {
		System.out.println("jop3");
		if (e.getSource() == SearcheField && e.getKeyChar() == '\n'){
			list.setModel(new ListModel_Countries(SearcheField.getText()));	
		}else if (e.getSource() == list && e.getKeyChar() == '\n'){
			openGalery(((JList)e.getComponent()).getSelectedIndex());
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	public void mouseClicked(MouseEvent e) {
		if (e.getSource().equals(list)){
			openGalery(((JList)e.getComponent()).getSelectedIndex());
		}else if (e.getSource() instanceof LittlePicturePanel){
			int imgID = ((LittlePicturePanel)e.getSource()).getId();
			photo_show_panel.setDirectory(((LittlePicturePanel)e.getSource()).getDir());
			photo_show_panel.setImage(imgID);
			galery_panel.setVisible(false);
			btnLeftPhoto.setEnabled(photo_show_panel.canDecrementId());
			btnRightPhoto.setEnabled(photo_show_panel.canIncrementId());
			photo_show_panel_container.setVisible(true);
			layeredPane.moveToFront(photo_show_panel_container);
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}
}
