package windows;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PhotoShowPanel extends JPanel {
	
	// TODO priority:medium show more info about photo (name, size, date, comment)
	// TODO priority:medium can run presentation (from galery also)
	// TODO priority:low zoom picture only to his real size
	// TODO priority:low with key you can move between picture also

	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private int border;
	private String name_of_directory;
	private int pictureId;
	private MainWindow parent;

	public PhotoShowPanel(MainWindow p) {
		parent = p;
		image = null;
		border = 5;
		pictureId= 0;
	}
	
	@Override
	public void setVisible(boolean flag){
		super.setVisible(flag);
		parent.setVisibleGaleryBtn(flag, "Show gallery "+name_of_directory);
	}
	
	public void setImage(int imgID){
		pictureId = imgID;
		showImageWithId();
	}

	private void showImageWithId(){
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
		String name = (name_of_directory_fp.listFiles()[pictureId]).getAbsolutePath();
		try {                
	        image = ImageIO.read(new File(name));
	     } catch (IOException ex) {
	          // handle exception...
	     }
	}
	
	public void setDirectory(String name){
		name_of_directory = name;
	}
	
	public void decrementId(){
		if (pictureId > 0){
			pictureId--;
		}
		showImageWithId();
	}
	
	public void incrementId(){
		pictureId++;
		showImageWithId();
	}
	
	public void resetId(){
		pictureId=0;
		showImageWithId();
	}
	
	public boolean canDecrementId(){
		return pictureId > 0;
	}
	
	public boolean canIncrementId(){
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
		return (pictureId < name_of_directory_fp.listFiles().length-1);
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (image == null) return;
        
        //System.out.println("image "+image.getWidth()+" "+image.getHeight());
        //System.out.println("frame "+this.getWidth()+" "+this.getHeight());
        double var = (double)image.getWidth()/image.getHeight();
        double invar = (double)(this.getBounds().width-2*border) / (this.getBounds().height-2*border);
        //System.out.println("vars "+var+" "+invar);
        
        if (var < invar){
        	int w = (int)((double)(this.getBounds().height-2*border)/(double)image.getHeight() * image.getWidth());
        	int del = (this.getBounds().width-w)/2;
        	//System.out.println("sol "+w+"("+del+")"+(this.getBounds().height-2*border));
            g.drawImage(image, border+del, border, w, this.getBounds().height-2*border, null);
            //System.out.println("1 "+w+" "+this.getBounds().height);
        }else{
        	int h = (int)((double)(this.getBounds().width-2*border)/(double)image.getWidth() * image.getHeight());
        	int del = (this.getBounds().height-h)/2;
        	//System.out.println("sol "+(this.getBounds().width-2*border)+h+"("+del+")");
            g.drawImage(image, border, border+del, this.getBounds().width-2*border, h, null);
            //System.out.println("2 "+this.getBounds().width+" "+h);
        }
    }
}
