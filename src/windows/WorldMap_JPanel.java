package windows;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class WorldMap_JPanel extends JPanel implements MouseListener, MouseWheelListener, MouseMotionListener{

	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private int startX, startY, endX, endY;
	private int moveX, moveY;
	private int tmpmoveX, tmpmoveY;
	private int bffmoveX, bffmoveY;
	private boolean isMoved;
	private int scaleStartX, scaleStartY, scaleEndX, scaleEndY;
	private MainWindow parent;
	
	private final int BORDER = -16777216;
	private final int WATER = -9596161;
	private final int VISITED = new Color(0,250,0).getRGB();
	private final int WHITE = -1;
	
	//private int pocitadlo_statu = 70; // for easily making coordinates of countries
	
	public WorldMap_JPanel(MainWindow p) {
		
			parent = p;
		
	       try {  
	          image = ImageIO.read(getClass().getResource("/world-map.png"));
	       } catch (IOException ex) {
	            // handle exception...
	       }
	       this.addMouseListener(this);
	       this.addMouseWheelListener(this);
	       this.addMouseMotionListener(this);
	       this.setBackground(new Color(109,146,255));
	       startX=0;
	       startY=0;
	       endX=this.getBounds().width;
	       endY=this.getBounds().height;
	       moveX = 0;
	       moveY = 0;
	       tmpmoveX = 0;
	       tmpmoveY = 0;
	       bffmoveX = 0;
	       bffmoveY = 0;
	       scaleStartX = 0;
	       scaleStartY = 0;
	       scaleEndX = 0;
	       scaleEndY = 0;
	       isMoved = false;
	    }

	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        //this.addMouseListener(this);
		    startX=0-moveX-tmpmoveX+scaleStartX;
		    startY=0-moveY-tmpmoveY+scaleStartY;
		    endX=this.getBounds().width+scaleEndX-scaleStartX;
		    endY=this.getBounds().height+scaleEndY-scaleStartY;
		    if (endX < 150) endX = 150;
		    if (endY < 150) endY = 150;
	        
	        // colorized countries, which have galery

	        ListModel_Countries listOfCountries = new ListModel_Countries();
	        File name_of_directory_fp = new File(MainWindow.path_to_photos);
	        DatabaseOfCountriesCoordination dbCountries = new DatabaseOfCountriesCoordination();
	        
	        if (name_of_directory_fp.listFiles() == null){
	        	name_of_directory_fp.mkdir();
	        }
	        int idCountry = 0;
	        Point finalCoordination = null;
	        for (int i = 0 ; i < name_of_directory_fp.listFiles().length ; i++){
	        	for (int h = 0 ; h < listOfCountries.getSize() ; h++){
	        		if (name_of_directory_fp.listFiles()[i].getName().equals(listOfCountries.getElementAt(h))){
	        			idCountry = h;
	        			break;
	        		}
	        	}
	        	
	        	List<Point> points = new LinkedList<Point>();
        		
	        	for (int h = 0 ; h < dbCountries.getLength() ; h++){
	        		if (dbCountries.getId(h) == idCountry){
	        			finalCoordination = dbCountries.getPoint(h);
	    	        	points.add(finalCoordination);
	            		image.setRGB(points.get(points.size()-1).x, points.get(points.size()-1).y, VISITED);
	        		}
	        	}
	        	// fill from position finalCoordination to black bounds
	        	if (finalCoordination == null){
	        		System.err.println("Error, interal error (4968)");
	        		continue;
	        	}
	        	
	        	while (points.size() != 0){
	        		if (image.getRGB(points.get(0).x-1, points.get(0).y) == WHITE){
	        			points.add(new Point(points.get(0).x-1, points.get(0).y));
		        		image.setRGB(points.get(0).x-1, points.get(0).y, VISITED);
	        		}
	        		if (image.getRGB(points.get(0).x+1, points.get(0).y) == WHITE){
	        			points.add(new Point(points.get(0).x+1, points.get(0).y));
		        		image.setRGB(points.get(0).x+1, points.get(0).y, VISITED);
	        		}
	        		if (image.getRGB(points.get(0).x, points.get(0).y-1) == WHITE){
	        			points.add(new Point(points.get(0).x, points.get(0).y-1));
		        		image.setRGB(points.get(0).x, points.get(0).y-1, VISITED);
	        		}
	        		if (image.getRGB(points.get(0).x, points.get(0).y+1) == WHITE){
	        			points.add(new Point(points.get(0).x, points.get(0).y+1));
		        		image.setRGB(points.get(0).x, points.get(0).y+1, VISITED);
	        		}
	        		points.remove(0);
	        	}
	        }
	        g.drawImage(image, startX, startY, endX, endY, null);
	    }
	    
	    public void resetView(){
		       startX=0;
		       startY=0;
		       endX=this.getBounds().width;
		       endY=this.getBounds().height;
		       moveX = 0;
		       moveY = 0;
		       tmpmoveX = 0;
		       tmpmoveY = 0;
		       bffmoveX = 0;
		       bffmoveY = 0;
		       scaleStartX = 0;
		       scaleStartY = 0;
		       scaleEndX = 0;
		       scaleEndY = 0;
		       isMoved = false;
		       this.repaint();
	    }

		public void mouseClicked(MouseEvent e) {
			// detect country by position of dblclick
			
			if (e.getButton() == MouseEvent.BUTTON3){
				// for easily making coordinations of countries
				//pocitadlo_statu++; 
				//ListModel_Countries lmc = new ListModel_Countries();
				//System.out.println("// "+lmc.getElementAt(pocitadlo_statu+new ListModel_Countries().getLength(2)));
			} else 
			if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1){
				DatabaseOfCountriesCoordination dbCountries = new DatabaseOfCountriesCoordination();
				boolean founded = false;
				int final_country = 0;
	
				int positionOnPictureX = (int)((e.getX()+moveX-scaleStartX)/(double)endX*image.getWidth());
				int positionOnPictureY = (int)((e.getY()+moveY-scaleStartY)/(double)endY*image.getHeight());
				
				//System.out.println(image.getRGB(positionOnPictureX, positionOnPictureY));
				if (image.getRGB(positionOnPictureX, positionOnPictureY) == BORDER){
			    	//System.out.println("you clicked into border");
			    	founded = true;
			    	return;
				}else if (image.getRGB(positionOnPictureX, positionOnPictureY) == WATER){
					//System.out.println("you clicked into water");
			    	founded = true;
			    	return;
				}else{				
					for (int i = 0 ; i < dbCountries.getLength() ; i++){
						int x0 = positionOnPictureX;
						int y0 = positionOnPictureY;
						int x1 = dbCountries.getPoint(i).x;
						int y1 = dbCountries.getPoint(i).y;
						
	    				if (x0 > x1){
	    					int buff = x0;
					    	x0 = x1;
					    	x1 = buff;
					    	
					    	buff = y0;
					    	y0 = y1;
					    	y1 = buff;
					    }
	    				 
	    				if (x0==x1 && y0!=y1) x0--; // in other case no line is controlled
						 
					    double deltax = x1 - x0;
					    double deltay = y1 - y0;
			    		double error = 0;
	    				double deltaerr = Math.abs(deltay / deltax);
					    
	    				boolean isClearWay = true;
	    				 
					    int y = y0;
					    for (int x = x0; x < x1 ; x++){
					    	if (!isClearWay || image.getRGB(x, y) == BORDER){
					    		isClearWay = false;
					    		break;
					    	}
					        error = error + deltaerr;
					        while (error >= 0.5){
						    	if (!isClearWay || image.getRGB(x, y) == BORDER){
						    		isClearWay = false;
						    		break;
						    	}
					            y = (int)(y + Math.signum(y1 - y0));
					            error = error - 1.0;
					        }
					    }
					    if (isClearWay){
					    	//System.out.println("you clicked into "+listOfCountries.getElementAt(dbCountries.getId(i)));
					    	final_country = dbCountries.getId(i);
					    	founded = true;
					    	break;
					    }
					}
				}
				
				if (!founded){
					// System.out.println("countries[count++] = new RecordCountryAndPosition(new Point("+positionOnPictureX+", "+positionOnPictureY+"),index+"+pocitadlo_statu+");"); // for easily making coordinatins of countries
					System.err.println("Error, not defined pixel on map");
				}else{
			        this.removeMouseListener(this);
			        this.removeMouseMotionListener(this);
			        this.removeMouseWheelListener(this);
					parent.openGalery(final_country);
				}
			}
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
			bffmoveX = e.getX();
			bffmoveY = e.getY();
			isMoved = true;
		}

		public void mouseDragged(MouseEvent e) {
			if (isMoved){
				tmpmoveX = bffmoveX-e.getX();
				tmpmoveY = bffmoveY-e.getY();
			}
			this.repaint();
		}

		public void mouseMoved(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
			moveX += tmpmoveX;
			moveY += tmpmoveY;
			tmpmoveX=0;
			tmpmoveY=0;
			isMoved = false;
			this.repaint();
		}

		public void mouseWheelMoved(MouseWheelEvent arg0) {
			
			// near - more zoome; far - little zoom
			int moveStepX = (this.getBounds().width+scaleEndX-scaleStartX)/10;
			int moveStepY = moveStepX/2; 
			if (arg0.getWheelRotation() == -1){ // up
				int positionOnPictureX = arg0.getX()+moveX-scaleStartX;
				int positionOnPictureY = arg0.getY()+moveY-scaleStartY;

				scaleStartX += - ((double)positionOnPictureX / (endX) * moveStepX);
				scaleEndX += ((double)(endX-positionOnPictureX)) / (endX) * moveStepX;
				scaleStartY += - ((double)positionOnPictureY / (endY) * moveStepY);
				scaleEndY += ((double)(endY-positionOnPictureY)) / (endY) * moveStepY;
			
			}else{ // down
				int positionOnPictureX = arg0.getX()+moveX-scaleStartX;
				int positionOnPictureY = arg0.getY()+moveY-scaleStartY;
				
				scaleStartX -= - ((double)positionOnPictureX / (endX) * moveStepX);
				scaleEndX -= ((double)(endX-positionOnPictureX)) / (endX) * moveStepX;
				scaleStartY -= - ((double)positionOnPictureY / (endY) * moveStepY);
				scaleEndY -= ((double)(endY-positionOnPictureY)) / (endY) * moveStepY;
				
			}
			this.repaint();
		}
}