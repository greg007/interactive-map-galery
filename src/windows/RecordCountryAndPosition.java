package windows;

import java.awt.Point;

public class RecordCountryAndPosition {
	public final Point source;
	public final int countrId;
	
	public RecordCountryAndPosition(Point p, int id) {
		source = p;
		countrId = id;
	}
}
