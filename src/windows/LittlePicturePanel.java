package windows;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class LittlePicturePanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private int border = 2;
	private int idPicture;
	private String directory;
	
	public LittlePicturePanel(String file, int id, String dir){
		directory = dir;
		idPicture=id;
		try {                
	        image = ImageIO.read(new File(file));
	     } catch (IOException ex) {
	          // handle exception...
	     }
	}
	
	public BufferedImage getImage(){
		return image;
	}
	
	public int getId(){
		return idPicture;
	}
	
	public String getDir(){
		return directory;
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //System.out.println("image "+image.getWidth()+" "+image.getHeight());
        //System.out.println("frame "+this.getWidth()+" "+this.getHeight());
        double var = (double)image.getWidth()/image.getHeight();
        double invar = (double)(this.getBounds().width-2*border) / (this.getBounds().height-2*border);
        //System.out.println("vars "+var+" "+invar);
        
        if (var < invar){
        	int w = (int)((double)(this.getBounds().height-2*border)/(double)image.getHeight() * image.getWidth());
        	int del = (this.getBounds().width-w)/2;
        	//System.out.println("sol "+w+"("+del+")"+(this.getBounds().height-2*border));
            g.drawImage(image, border+del, border, w, this.getBounds().height-2*border, null);
            //System.out.println("1 "+w+" "+this.getBounds().height);
        }else{
        	int h = (int)((double)(this.getBounds().width-2*border)/(double)image.getWidth() * image.getHeight());
        	int del = (this.getBounds().height-h)/2;
        	//System.out.println("sol "+(this.getBounds().width-2*border)+h+"("+del+")");
            g.drawImage(image, border, border+del, this.getBounds().width-2*border, h, null);
            //System.out.println("2 "+this.getBounds().width+" "+h);
        }
    }
	
}
