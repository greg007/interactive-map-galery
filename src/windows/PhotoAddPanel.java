package windows;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import static java.nio.file.StandardCopyOption.*;
public class PhotoAddPanel extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	JPanel upsidePanel;
	JPanel downsidePanel;
	private String directory;
	private MainWindow parent;
	// TODO priority:low create form for modifying galery 
	
	public PhotoAddPanel(MainWindow p) {
		parent = p;
		
		this.setLayout(new BorderLayout());
		upsidePanel = new JPanel();
		this.add(upsidePanel, BorderLayout.NORTH);
		upsidePanel.setLayout(new GridLayout(1, 2));
		
		JLabel lab1 = new JLabel("Add photo(s) into this galery");
		upsidePanel.add(lab1);
		JButton but1= new JButton("Add");
		upsidePanel.add(but1);
		but1.addActionListener(this);
		
		JScrollPane scrollPane = new JScrollPane();
		downsidePanel = new JPanel();
		scrollPane.setViewportView(downsidePanel);
		this.add(scrollPane, BorderLayout.CENTER);
		
		downsidePanel.setLayout(new GridLayout(2, 1));
		
		this.repaint();
	}

	public void setVisible(boolean flag){
		super.setVisible(flag);
		parent.setVisibleGaleryBtn(flag, "");
		/*downsidePanel.removeAll();

		try{
			File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+directory);

			System.out.println(MainWindow.path_to_photos+"/"+directory);
			for (int i = 0 ; i < name_of_directory_fp.listFiles().length ; i++){
				PhotoModifierRecord_Panel pmrp = new PhotoModifierRecord_Panel(name_of_directory_fp.listFiles()[i].getAbsolutePath());
				downsidePanel.add(pmrp);
				
				//break;
			}
		}catch(NullPointerException e){
			System.out.println("ignoruju chybu ");
		}
		this.repaint();*/
	}
	
	public void setDirectory(String dir){
		//System.out.println("nastavuju dir ");
		directory = MainWindow.path_to_photos+"/"+dir;
	}
	
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(true);
		fc.showOpenDialog(this);
		for (int i = 0 ; i < fc.getSelectedFiles().length ; i++){
			//System.out.println(fc.getSelectedFiles()[i].getName());
			//System.out.println(directory);
			
			try {
				File ff = new File(directory+"/"+fc.getSelectedFiles()[i].getName());
				//ff.createNewFile();
				//System.out.println(fc.getSelectedFiles()[i].toPath());
				Files.copy(fc.getSelectedFiles()[i].toPath(),ff.toPath(), COPY_ATTRIBUTES);
			} catch (IOException e1) {
				//e1.printStackTrace();
				System.out.println(e1);
			}
			
		}
	}
}
