package windows;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PhotoModifierRecord_Panel extends JPanel{
	private static final long serialVersionUID = 1L;
	private BufferedImage image;

	public PhotoModifierRecord_Panel(String img_name) {
       try {                
	          image = ImageIO.read(new File(img_name));
	       } catch (IOException ex) {
	            // handle exception...
	       }
       this.setLayout(new BorderLayout());
       //this.setBackground(Color.GREEN);
       
	}
	
	protected void paintComponent(Graphics g) {
		   super.paintComponent(g);
		   
		   int ww = (int)((double)image.getWidth()/image.getHeight() * this.getHeight());
		   
		   g.drawImage(image,  0, 0, ww, this.getHeight(), null);
	}
		
}
