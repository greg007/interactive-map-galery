package windows;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.io.File;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Constraint_panel extends JPanel{
	
	// TODO priority:low more efficiently showing galery
	// TODO priority:low show somewhere count of photos
	// TODO priority:low mark focused picture
	// TODO priority:low can choose picture by keys

	private static final long serialVersionUID = 1L;
	
	private String name_of_directory;
	private JLabel lblnothink;
	private int startID;
	private int size_of_field = 4;
	
	private MainWindow parent;
	
	public Constraint_panel(MainWindow p) {
		
		parent = p;
		lblnothink = new JLabel("");
		this.add(lblnothink, BorderLayout.CENTER);
		lblnothink.setVerticalAlignment(SwingConstants.CENTER);
		lblnothink.setHorizontalAlignment(SwingConstants.CENTER);
		startID = 0;
	}
	
	public void setDirectory(String name){
		name_of_directory = name;
	}
	
	public void decrementStartId(){
		if (startID > 0){
			startID-=size_of_field*size_of_field;
		}
	}
	
	public void incrementStartId(){
		startID+=size_of_field*size_of_field;
	}
	
	public void resetStartId(){
		startID=0;
	}
	
	public boolean canDecrementStartId(){
		return startID > 0;
	}
	
	public boolean canIncrementStartId(){
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
		return (startID+size_of_field*size_of_field < name_of_directory_fp.listFiles().length);
	}
	
	public int getIdOfPicture(String name){
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
		for (int i =  0 ; i < name_of_directory_fp.listFiles().length ; i++){
			if (name_of_directory_fp.listFiles()[i].equals(name)){
				return i;
			}
		}
		return -1;
	}

	@Override
    protected void paintComponent(Graphics g) {
		
		this.removeAll();
		
		File name_of_directory_fp = new File(MainWindow.path_to_photos+"/"+name_of_directory);
			
		if (name_of_directory_fp.exists()) {
			
			if (name_of_directory_fp.listFiles().length == 0){
				this.setLayout(new BorderLayout(0, 0));
				lblnothink.setVisible(true);
				this.add(lblnothink, BorderLayout.CENTER);
				lblnothink.setText("This galery has not any photo yet.");
			}else{
				// show galery
				lblnothink.setVisible(false);
				
				
				this.setLayout(null);
				
				int all_width = this.getBounds().width/size_of_field;
				int all_height = this.getBounds().height/size_of_field;
				for (int y = 0 ; y < size_of_field ; y++){
					for (int x = 0 ; x < size_of_field ; x++){
						if (startID+y*size_of_field+x < name_of_directory_fp.listFiles().length){
							//System.out.println("na pozici "+x*all_width+"("+all_width+"):"+y*all_height+"("+all_height+") printuju "+name_of_directory_fp.listFiles()[startId+y+size_of_field*x].getAbsolutePath());
							LittlePicturePanel cell = new LittlePicturePanel(name_of_directory_fp.listFiles()[startID+y*size_of_field+x].getAbsolutePath(),startID+y*size_of_field+x,name_of_directory);
							cell.setBounds(x*all_width, y*all_height, all_width, all_height);
							this.add(cell);
							cell.addMouseListener(parent);
						}else{
							break;
						}
					}
				}
				for (int i = 0 ; i < name_of_directory_fp.listFiles().length ; i++){
					//System.out.println(name_of_directory_fp.listFiles()[i].getName());
				}
				
			}
		}else{
			// if not found galery
			this.setLayout(new BorderLayout(0, 0));
			lblnothink.setVisible(true);
			this.add(lblnothink, BorderLayout.CENTER);
			lblnothink.setText("Galery has not been created for this country yet.");
		}
	}
}
