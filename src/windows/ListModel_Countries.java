package windows;

import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractListModel;

public class ListModel_Countries extends AbstractListModel<String>{

	private static final long serialVersionUID = 1L;
	
	// remove countries with smallest countries  like australia and usa
	String europe = " (Europe)";
	String asia = " (Asia)";
	String australia = " (Australia)";
	String america = " (America)";
	String africa = " (Africa)";
	
	public  String [] europe_countries = new String[] {
			"Albania",
			"Andorra",
			"Armenia",
			"Austria",
			"Azerbaijan",
			"Belarus",
			"Belgium",
			"Bosnia and Herzegovina",
			"Bulgaria",
			"Croatia",
			"Cyprus",
			"Czech Republic",
			"Denmark",
			"Estonia",
			"Finland",
			"France",
			"Georgia",
			"Germany",
			"Greece",
			"Hungary",
			"Iceland",
			"Ireland",
			"Italy",
			"Kazakhstan",
			"Kosovo",
			"Latvia",
			"Liechtenstein",
			"Lithuania",
			"Luxembourg",
			"Macedonia",
			"Malta",
			"Moldova",
			"Monaco",
			"Montenegro",
			"Netherlands",
			"Norway",
			"Poland",
			"Portugal",
			"Romania",
			"Russia",
			"San Marino",
			"Serbia",
			"Slovakia",
			"Slovenia",
			"Spain",
			"Sweden",
			"Switzerland",
			"Turkey",
			"Ukraine",
			"United Kingdom",
			"Vatican City"
			};
	public  String [] asia_countries = new String[]{
			"Afghanistan",
			"Bahrain",
			"Bangladesh",
			"Bhutan",
			"Brunei",
			"Cambodia",
			"China",
			"India",
			"Indonesia",
			"Iran",
			"Iraq",
			"Israel",
			"Japan",
			"Jordan",
			"Kuwait",
			"Kyrgyzstan",
			"Laos",
			"Lebanon",
			"Malaysia",
			"Maldives",
			"Mongolia",
			"Myanmar (Burma)",
			"Nepal",
			"North Korea",
			"Oman",
			"Pakistan",
			"Palestine",
			"Philippines",
			"Qatar",
			"Saudi Arabia",
			"Singapore",
			"South Korea",
			"Sri Lanka",
			"Syria",
			"Taiwan",
			"Tajikistan",
			"Thailand",
			"Timor-Leste",
			"Turkmenistan",
			"United Arab Emirates",
			"Uzbekistan",
			"Vietnam",
			"Yemen"
	};
	public  String [] america_countries = new String[] {
			"Antigua and Barbuda",
			"Bahamas",
			"Barbados",
			"Belize",
			"Canada",
			"Costa Rica",
			"Cuba",
			"Dominica",
			"Dominican Republic",
			"El Salvador",
			"Grenada",
			"Guatemala",
			"Haiti",
			"Honduras",
			"Jamaica",
			"Mexico",
			"Nicaragua",
			"Panama",
			"St. Kitts and Nevis",
			"St. Lucia",
			"St. Vincent and The Grenadines",
			"Trinidad and Tobago",
			"Alabama (USA)",
			"Alaska (USA)",
			"Arizona (USA)",
			"Arkansas (USA)",
			"California (USA)",
			"Colorado (USA)",
			"Connecticut (USA)",
			"Delaware (USA)",
			"Florida (USA)",
			"Georgia (USA)",
			"Hawaii (USA)",
			"Idaho (USA)",
			"Illinois (USA)",
			"Indiana (USA)",
			"Iowa (USA)",
			"Kansas (USA)",
			"Kentucky (USA)",
			"Louisiana (USA)",
			"Maine (USA)",
			"Maryland (USA)",
			"Massachusetts (USA)",
			"Michigan (USA)",
			"Minnesota (USA)",
			"Mississippi (USA)",
			"Missouri (USA)",
			"Montana (USA)",
			"Nebraska (USA)",
			"Nevada (USA)",
			"New Hampshire (USA)",
			"New Jersey (USA)",
			"New Mexico (USA)",
			"New York (USA)",
			"North Carolina (USA)",
			"North Dakota (USA)",
			"Ohio (USA)",
			"Oklahoma (USA)",
			"Oregon (USA)",
			"Pennsylvania (USA)",
			"Rhode Island (USA)",
			"South Carolina (USA)",
			"South Dakota (USA)",
			"Tennessee (USA)",
			"Texas (USA)",
			"Utah (USA)",
			"Vermont (USA)",
			"Virginia (USA)",
			"Washington (USA)",
			"West Virginia (USA)",
			"Wisconsin (USA)",
			"Wyoming (USA)",
			"Greenland Kalaallit Nunaat",
			"Argentina",
			"Bolivia",
			"Brazil",
			"Chile",
			"Colombia",
			"Ecuador",
			"Guyana",
			"Paraguay",
			"Peru",
			"Suriname",
			"Uruguay",
			"Venezuela",
			"Falkland Islands",
			"French Guiana",
			"South Georgia and South Sandwich Islands"
	};
	public  String [] australia_countries = new String [] {
			"Western Australia (Australia)",
			"Northern Territory (Australia)",
			"South Australia (Australia)",
			"Queensland (Australia)",
			"New South Wales (Australia)",
			"Victoria (Australia)",
			"Tasmania (Australia)",
			"Fiji",
			"Kiribati",
			"Marshall Islands",
			"Micronesia",
			"Nauru",
			"New Zealand",
			"Palau",
			"Papua New Guinea",
			"Samoa",
			"Solomon Islands",
			"Tonga",
			"Tuvalu",
			"Vanuatu"
	};
	public  String [] africa_countries = new String [] {
			"Algeria",
			"Angola",
			"Benin",
			"Botswana",
			"Burkina Faso",
			"Burundi",
			"Cabo Verde",
			"Cameroon",
			"Central African Republic",
			"Chad",
			"Comoros",
			"Republic of the Congo",
			"Democratic Republic of the Congo",
			"Cote d'Ivoire",
			"Djibouti",
			"Egypt",
			"Equatorial Guinea",
			"Eritrea",
			"Ethiopia",
			"Gabon",
			"Gambia",
			"Ghana",
			"Guinea",
			"Guinea-Bissau",
			"Kenya",
			"Lesotho",
			"Liberia",
			"Libya",
			"Madagascar",
			"Malawi",
			"Mali",
			"Mauritania",
			"Mauritius",
			"Morocco",
			"Mozambique",
			"Namibia",
			"Niger",
			"Nigeria",
			"Rwanda",
			"Sao Tome and Principe",
			"Senegal",
			"Seychelles",
			"Sierra Leone",
			"Somalia",
			"South Africa",
			"South Sudan",
			"Sudan",
			"Swaziland",
			"Tanzania",
			"Togo",
			"Tunisia",
			"Uganda",
			"Zambia",
			"Zimbabwe",
			"Western Sahara"
	};
	
	String[] values;
	
	public ListModel_Countries(){
		init();
	}
	
	public ListModel_Countries(String text) {
		init();
		
		List<String> vals = new LinkedList<String>();
		
		for (int i = 0 ; i < values.length ; i++){
			if (values[i].contains(text)){
				vals.add(values[i]);
			}
		}
		
		values = new String[vals.size()];
		values = vals.toArray(new String[0]);
		
	}
	
	private void init(){

		marker(europe_countries,europe);
		special_marker();
		marker(asia_countries,asia);
		marker(america_countries,america);
		marker(africa_countries,africa);
		marker(australia_countries,australia);
		
		values = concat(europe_countries, asia_countries);
		values = concat(values, america_countries);
		values = concat(values, australia_countries);
		values = concat(values, africa_countries);
	}

	private void marker(String[] field , String mark){
		for (int i = 0 ; i < field.length ; i++){
			field[i] += mark;
			//System.out.println(field[i]);
		}
	}
	
	private void special_marker(){
		// mark questionable territory
		// asia_field not contain questionable territory (is europe or asia like russia)

		europe_countries[2] += asia; // Armenia
		europe_countries[4] += asia; // Azerbaijan
		europe_countries[10] += asia; // Cyprus
		europe_countries[16] += asia; // Georgia
		europe_countries[23] += asia; // Kazakhstan 
		europe_countries[39] += asia; // Russia 
		europe_countries[47] += asia; // Turkey 
	}
	
	public int getLength(int i){
		switch (i) {
		case 0:
			return 0;
		case 1:
			return europe_countries.length;
		case 2:
			return europe_countries.length+asia_countries.length;
		case 3:
			return europe_countries.length+asia_countries.length+america_countries.length;
		case 4:
			return europe_countries.length+asia_countries.length+america_countries.length+australia_countries.length;
		case 5:
			return europe_countries.length+asia_countries.length+america_countries.length+australia_countries.length+africa_countries.length;
		default:
			return 0;
		}
	}
	
	public static String[] concat(String[] a, String[] b) {
		   int aLen = a.length;
		   int bLen = b.length;
		   String[] c= new String[aLen+bLen];
		   System.arraycopy(a, 0, c, 0, aLen);
		   System.arraycopy(b, 0, c, aLen, bLen);
		   return c;
		}
	
	public int getSize() {
		return values.length;
	}
	public String getElementAt(int index) {
		return values[index];
	}

}
